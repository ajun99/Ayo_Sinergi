<div class="container">
  <div class="row justify-content-center" style="margin-top:100px;">
  </div>
</div>

<!-- Footer -->
<footer class="page-footer font-small teal pt-4 mt-5 info-color-dark">

  <!-- Footer Text -->
  <div class="container-fluid text-center text-md-left mb-5">

    <!-- Grid row -->
    <div class="row justify-content-md-center">

      <!-- Grid column -->
      <div class="col-md-4 mt-md-0 mt-5">

        <!-- Content -->
        <h4 class="text-uppercase font-weight-bold mb-4">Contact</h4><hr />
        
        <p><i class="fas fa-map-marker-alt pr-2"></i> Gegunung, Tirtohargo, Kec. Kretek, Bantul, Daerah Istimewa Yogyakarta 55772</p>
        <p><i class="fas fa-phone-volume pr-2"></i> +6285228802828</p>
        <p><i class="far fa-envelope pr-2"></i> ayosinergi@gmail.com</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-4">

      <!-- Grid column -->
      <div class="col-md-4 mb-md-0 mb-3">

        <!-- Content -->
        <h4 class="text-uppercase font-weight-bold mb-4">Tentang</h4><hr />
        <p><b>Ayo Sinergi</b> merupakan platform untuk mempertemukan orang-orang yang mempunyai ide, yang ingin menjadi investor, dan
        tenaga programmer yang siap untuk mengeksekusinya.</p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Text -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© <?= date('Y') ?> Copyright:
    <a href="https://ayosinergi.com"> AyoSinergi.com</a>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer --> 
 <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.7/js/mdb.min.js"></script>

    <script>
      $('.custom-file-input').on('change', function() {
          let filename = $(this).val().split('\\').pop();
          $(this).next('.custom-file-label').addClass("selected").html(filename);
      });
  
      $('.form-check-input').on('click', function () {
          const menuId = $(this).data('menu');
          const roleId = $(this).data('role');

          $.ajax({
              url: "<?= base_url('admin/changeaccess'); ?>",
              type: 'post',
              data: {
                  menuId: menuId,
                  roleId: roleId
              },
              success: function() {
                  document.location.href = "<?= base_url('admin/roleaccess/'); ?>" + roleId;
              }
          });
      });
    </script>
</body>
</html>