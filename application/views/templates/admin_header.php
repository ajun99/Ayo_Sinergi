<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title; ?></title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.7/css/mdb.min.css" rel="stylesheet">
</head>
<body>

<!--Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark info-color-dark">
  <a class="navbar-brand" href="<?= base_url('auth'); ?>">AyoSinergi</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
    aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pesan</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Telusuri</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('admin/index') ?>">Admin Menu</a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto nav-flex-icons">
    <form class="form-inline ">
    <input class="form-control form-control-sm mr-3 w-75" type="text" placeholder="Cari"
        aria-label="Search">
    <i class="fas fa-search" aria-hidden="true"></i>
    </form>
      <li class="nav-item avatar dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">
          <img src="<?= base_url('assets/img/') . $user['image']; ?>" class="rounded-circle z-depth-0 img-fluid" width="35px"
            alt="avatar image">
        </a>
        <div class="dropdown-menu dropdown-menu-lg-right dropdown-secondary"
          aria-labelledby="navbarDropdownMenuLink-55">
          <a class="dropdown-item" href="<?= base_url('user/index'); ?>">My Profile</a>
          <a class="dropdown-item" href="<?= base_url('auth/logout'); ?>">Logout</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->