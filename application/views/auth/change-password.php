<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 mt-5">
            <!-- Default form register -->
            <form class="text-center border border-light p-5 user" method="post" action="<?= base_url('auth/changepassword'); ?>">

                <p class="h4 mb-4">Change your password for</p>
                <p class="h4 mb-4"><?= $this->session->userdata('reset_email'); ?></p>

                <div class="flash-message">
                    <?= $this->session->flashdata('message'); ?>
                </div>

                <!-- Password -->
                <input type="password" id="password1" name="password1" class="form-control user" placeholder="Password">
                <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                
                <!-- Repeat Password -->
                <input type="password" id="password2" name="password2" class="form-control user" placeholder="Repeat password">
                <?= form_error('password2', '<small class="text-danger pl-3">', '</small>'); ?>
                <!-- Change password button -->
                <button class="btn btn-info my-4 btn-block" type="submit">Change Password</button>
            </div>

            </form>
            <!-- Default form register -->
        </div>
    </div>
</div>