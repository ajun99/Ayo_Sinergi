<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-lg-7 mt-5">
            <!-- Default form register -->
            <form class="text-center border border-light p-5 user" method="post" action="<?= base_url('auth/registration') ?>">

            <p class="h4 mb-4">Sign up</p>

            <!-- Full name -->
            <div class="form-group">
                <input type="text" id="name" name="name" class="form-control user mb-4" placeholder="Full name">
                <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
            <!-- E-mail -->
            <div class="form-group">
                <input type="text" id="email" name="email" class="form-control user mb-4" placeholder="E-mail">
                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
            </div>
            <div class="form-row mb-4">
                <!-- Password -->
                <div class="col">
                    <input type="password" id="password1" name="password1" class="form-control user" placeholder="Password">
                    <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
                <!-- Password -->
                <div class="col">
                    <input type="password" id="password2" name="password2" class="form-control user" placeholder="Repeat password">
                </div>
            </div>

            <!-- Sign up button -->
            <button class="btn btn-info my-4 btn-block" type="submit">Sign up</button>

            <hr>

            <!-- Register -->
            <p class="small">Have an account?
                <a href="<?= base_url('auth') ?>">Log in</a>
            </p>

            <!-- Register -->
            <p class="small">Forgot your password?
                <a href="<?= base_url('auth/forgotpassword') ?>">Reset</a>
            </p>

            </form>
            <!-- Default form register -->
        </div>
    </div>
</div>