
    <div class="container mt-5 mb-5">
        <div class="row justify-content-center">
            <div class="col-lg-7 mt-5 mb-5" style="background-color:white;">
                <!-- Default form login -->
                <form class="user text-center border border-light p-5" method="post" action="<?= base_url('auth'); ?>">

                <p class="h4 mb-4">Login</p>
                
                <div class="flash-message">
                    <?= $this->session->flashdata('message'); ?>
                </div>

                <!-- Email -->
                <div class="form-group">
                    <input type="text" id="email" name="email" class="form-control user mb-4" placeholder="Enter email address" value="<?= set_value('email'); ?>">
                    <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>

                <!-- Password -->
                <div class="form-group">
                    <input type="password" id="password" name="password" class="form-control user mb-4" placeholder="Password">
                    <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
                </div>
                <!-- Sign in button -->
                <button class="btn btn-info btn-block my-4" type="submit">Login</button>

                <hr>

                <!-- Register -->
                <p class="small">Not a member?
                    <a href="<?= base_url('auth/registration'); ?>">Register</a>
                </p>

                <!-- Register -->
                <p class="small">Forgot your password?
                    <a href="<?= base_url('auth/forgotpassword') ?>">Reset</a>
                </p>

                </form>
                <!-- Default form login -->

                
            </div>
        </div>
    </div>
    
