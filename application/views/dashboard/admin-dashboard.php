<!--Carousel Wrapper-->
<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-2" data-slide-to="1"></li>
    <li data-target="#carousel-example-2" data-slide-to="2"></li>
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <img class="d-block w-100" src="<?= base_url('assets'); ?>/img/0.png"
          alt="First slide">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption mb-5 w-50 h-50" style="background-color:rgba(3, 169, 244, 0.5);">
        <h1 class="h1-responsive text-left pl-5 font-weight-bold">Ayo Sinergi</h1>
        <p class="text-left pl-5">ayo sinergi</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="<?= base_url('assets'); ?>/img/1.png"
          alt="Second slide">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption mb-5 w-50 h-50" style="background-color:rgba(3, 169, 244, 0.5);">
        <h1 class="h1-responsive text-left pl-5 font-weight-bold">Ayo Sinergi</h1>
        <p class="text-left pl-5">ayo sinergi</p>
      </div>
    </div>
    <div class="carousel-item"text>
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="<?= base_url('assets'); ?>/img/2.png"
          alt="Third slide">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption mb-5 w-50 h-50" style="background-color:rgba(3, 169, 244, 0.5);">
        <h1 class="h1-responsive text-left pl-5 font-weight-bold">Ayo Sinergi</h1>
        <p class="text-left pl-5">ayo sinergi</p>
      </div>
    </div>
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->

<div class="container mt-5">
  <div class="row">
    <div class="col-lg-12">
      <h2 class="text-center">Ayo Bersinergi dengan Ayo Sinergi</h2>
    </div>
  </div>
</div>

<!-- Card -->
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-lg-4">
      <div class="card">

      <!-- Card image -->
      <div class="view overlay">
        <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!-- Card content -->
      <div class="card-body">

        <!-- Title -->
        <h4 class="card-title">Contoh Ide</h4>
        <!-- Text -->
        <p class="card-text">Ini adalah text deskripsi daripada contho ide yang disubmit di website ini untuk Bersinergi.</p>
        <!-- Button -->
        <a href="#" class="btn btn-primary">Detail</a>

        <a href="#" class="btn btn-primary">Pesan</a>

      </div>

      </div>
    </div>

    <div class="col-lg-4">
      <div class="card">

      <!-- Card image -->
      <div class="view overlay">
        <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!-- Card content -->
      <div class="card-body">

        <!-- Title -->
        <h4 class="card-title">Contoh Ide</h4>
        <!-- Text -->
        <p class="card-text">Ini adalah text deskripsi daripada contho ide yang disubmit di website ini untuk Bersinergi.</p>
        <!-- Button -->
        <a href="#" class="btn btn-primary">Detail</a>

        <a href="#" class="btn btn-primary">Pesan</a>

      </div>

      </div>
    </div>

    <div class="col-lg-4">
      <div class="card">

      <!-- Card image -->
      <div class="view overlay">
        <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!-- Card content -->
      <div class="card-body">

        <!-- Title -->
        <h4 class="card-title">Contoh Ide</h4>
        <!-- Text -->
        <p class="card-text">Ini adalah text deskripsi daripada contho ide yang disubmit di website ini untuk Bersinergi.</p>
        <!-- Button -->
        <a href="#" class="btn btn-primary">Detail</a>

        <a href="#" class="btn btn-primary">Pesan</a>

      </div>

      </div>
    </div>
  </div>
</div>
<!-- Card -->

<!-- Card -->
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-lg-4">
      <div class="card">

      <!-- Card image -->
      <div class="view overlay">
        <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!-- Card content -->
      <div class="card-body">

        <!-- Title -->
        <h4 class="card-title">Contoh Ide</h4>
        <!-- Text -->
        <p class="card-text">Ini adalah text deskripsi daripada contho ide yang disubmit di website ini untuk Bersinergi.</p>
        <!-- Button -->
        <a href="#" class="btn btn-primary">Detail</a>

        <a href="#" class="btn btn-primary">Pesan</a>

      </div>

      </div>
    </div>

    <div class="col-lg-4">
      <div class="card">

      <!-- Card image -->
      <div class="view overlay">
        <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!-- Card content -->
      <div class="card-body">

        <!-- Title -->
        <h4 class="card-title">Contoh Ide</h4>
        <!-- Text -->
        <p class="card-text">Ini adalah text deskripsi daripada contho ide yang disubmit di website ini untuk Bersinergi.</p>
        <!-- Button -->
        <a href="#" class="btn btn-primary">Detail</a>

        <a href="#" class="btn btn-primary">Pesan</a>

      </div>

      </div>
    </div>

    <div class="col-lg-4">
      <div class="card">

      <!-- Card image -->
      <div class="view overlay">
        <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
        <a href="#!">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!-- Card content -->
      <div class="card-body">

        <!-- Title -->
        <h4 class="card-title">Contoh Ide</h4>
        <!-- Text -->
        <p class="card-text">Ini adalah text deskripsi daripada contho ide yang disubmit di website ini untuk Bersinergi.</p>
        <!-- Button -->
        <a href="#" class="btn btn-primary">Detail</a>

        <a href="#" class="btn btn-primary">Pesan</a>

      </div>

      </div>
    </div>
  </div>
</div>
<!-- Card -->