<div class="container">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 mt-4 text-center"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg-8">
            <?= $this->session->flashdata('message'); ?>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-right">
                <img class="w-50" src="<?= base_url('assets/img/') . $user['image']; ?>"><br>
                <h3><a href="<?= base_url('user/edit') ?>" class="badge badge-primary">Edit Profile</a></h3>
            </div>
            <div class="col-lg-6">
                <h5 class="card-title"><?= $user['name']; ?></h5>
                <p class="card-text"><?= $user['email']; ?></p>
                <p class="card-text"><small class="text-muted">
                Member since <?= date('d F Y', $user['date_created']); ?></small></p>   
            </div>
        </div>
    </div>
</div>